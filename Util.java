import java.io.*;

import java.util.*;

import com.google.common.base.*;

import cern.colt.matrix.tbit.*;
import cern.colt.matrix.tdouble.*;
import cern.colt.matrix.tdouble.algo.*;
import cern.colt.matrix.tdouble.impl.*;
import cern.jet.math.tdouble.*;
import cern.jet.random.tdouble.engine.*;
import cern.colt.function.tdouble.*;

public class Util {
	public static MersenneTwister64 RandomEngine = new MersenneTwister64();
	
		public static DoubleMatrix1D SteadyStateFromStocashticMatrix(DoubleMatrix2D transitonMatrix) {
		

		DenseDoubleAlgebra calc = new DenseDoubleAlgebra();
		
		DoubleMatrix2D temp = new DenseDoubleMatrix2D(transitonMatrix.rows(),transitonMatrix.columns()); 
		temp.assign(transitonMatrix);				
		 

		double error = Double.MAX_VALUE;

		while (error >Parameters.EPSILON) {
			DoubleMatrix2D old=temp.copy();
			temp = calc.mult(old, old);			
			error = Util.Error(temp, old);
		}
		
		
		return temp.viewRow(0).copy();

	}
	
	public static DoubleMatrix1D  Range(final double start, final double finish) {
		return Util.Range(start, finish, 1.0);
	}

	public static DoubleMatrix1D  Range(final double start, final double finish,
			final double jump) {
		final double[] ret = new double[(int) ((finish - start) / jump)];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = start + i * jump;
		}
		return new DenseDoubleMatrix1D(ret);
	}
	
		public static void WriteMatrixToCSV(DoubleMatrix2D matrix, String fileName){
		try {
			BufferedWriter bw= new BufferedWriter(new FileWriter(fileName));
			Iterable<String> lines=com.google.common.base.Splitter.on("\n").split(matrix.toString());
			Iterator<String> itr=lines.iterator();
			itr.next();//remove header
			while(itr.hasNext()){
				bw.write(Joiner.on(',').join(Splitter.on(" ").trimResults().omitEmptyStrings().split(itr.next())));
				bw.newLine();
			}
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void tic() {
		Util.time = System.currentTimeMillis();
	}

	public static void toc() {
		System.out.println(System.currentTimeMillis() - Util.time);
	}
	
		public static double Error(DoubleMatrix1D A, DoubleMatrix1D B) {
		DoubleMatrix1D E = A.copy();
		E.assign(B, DoubleFunctions.minus);
		E.assign(DoubleFunctions.abs);
		return E.zSum();
	}
	
		public static DoubleMatrix1D cumsum(DoubleMatrix1D vector) {
		DoubleMatrix1D ret = new DenseDoubleMatrix1D((int) vector.size());
		ret.set(0, vector.get(0));
		for (int i = 1; i < vector.size(); i++)
			ret.set(i, ret.get(i - 1) + vector.get(i));
		return ret;
	}
	
	class FisherTransformation implements DoubleFunction {

	private static final double e = DoubleFunctions.exp.apply(1);

	@Override
	public double apply(double k) {
		double ret = DoubleFunctions.lg.apply(1 + k, e)
				- DoubleFunctions.lg.apply(1 - k, e);
		return 0.5 * ret;
	}

}

class ReverseFisherTransformation implements DoubleFunction {

	
	@Override
	public double apply(double z) {
		return DoubleFunctions.div.apply(DoubleFunctions.exp.apply(2 * z) - 1,
				DoubleFunctions.exp.apply(2 * z) + 1);
	}

}


	

}